package top.team.wrapper_api.dto.response.dataResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataBank {
    private String bankCode;
    private String bankName;
    private  String channelsSupported;
    private String accVerAvailable;
    private String ifsc;
    private String ifscStatus;
}
