package top.team.wrapper_api.dto.response.dataResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.RecipientFetchResponse;

import java.util.List;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRecipientFetchAll {

    private List<DataRecipientFetch>  recipientList;

}
