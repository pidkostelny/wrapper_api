package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataBankList;
import top.team.wrapper_api.dto.response.dataResponse.DataGenericOtp;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GenericBankListResponse {
    private String errorMsg;
    private String errorCode;
    private DataBankList data;
}
