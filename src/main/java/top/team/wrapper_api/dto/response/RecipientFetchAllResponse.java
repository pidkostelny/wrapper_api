package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataCustomerFetch;
import top.team.wrapper_api.dto.response.dataResponse.DataRecipientFetchAll;

import java.util.List;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecipientFetchAllResponse {

    private String errorMsg;
    private String errorCode;
    private DataRecipientFetchAll data;
}
