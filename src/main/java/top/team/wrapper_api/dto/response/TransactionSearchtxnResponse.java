package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataTransaction;
import top.team.wrapper_api.dto.response.dataResponse.DataTransactionSearchtxn;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionSearchtxnResponse {
    private String errorMsg;
    private String errorCode;
    private DataTransactionSearchtxn data;
}
