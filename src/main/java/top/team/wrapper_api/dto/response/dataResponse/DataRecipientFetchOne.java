package top.team.wrapper_api.dto.response.dataResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRecipientFetchOne {
    private String udf2;
    private String udf1;
    private String recipientName;
    private String mobileNo;
}