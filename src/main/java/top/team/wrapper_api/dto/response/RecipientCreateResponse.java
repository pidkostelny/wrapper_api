package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataCustomerFetch;
import top.team.wrapper_api.dto.response.dataResponse.DataRecipientCreate;

import java.time.LocalDate;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecipientCreateResponse {

    private String errorMsg;
    private String errorCode;
    private DataRecipientCreate data;

}
