package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataTransactionPendingtxnlist;
import top.team.wrapper_api.dto.response.dataResponse.DataTransactionSearchtxn;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionPendingtxnlistResponse {
    private String errorMsg;
    private String errorCode;
    private DataTransactionPendingtxnlist data;
}
