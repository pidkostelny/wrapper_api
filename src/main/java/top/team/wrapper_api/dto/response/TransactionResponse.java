package top.team.wrapper_api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataTransaction;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionResponse {
    private String errorMsg;
    private String errorCode;
    private DataTransaction data;
}
