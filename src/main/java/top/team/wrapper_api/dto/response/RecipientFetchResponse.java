package top.team.wrapper_api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import top.team.wrapper_api.dto.response.dataResponse.DataRecipientFetch;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecipientFetchResponse {

    private String errorMsg;
    private String errorCode;
    private DataRecipientFetch data;

}
