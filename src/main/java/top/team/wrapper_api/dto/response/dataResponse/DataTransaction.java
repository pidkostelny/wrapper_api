package top.team.wrapper_api.dto.response.dataResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataTransaction {
    private String customerId;
    private String clientRefId;
    private String txnId;
    private String name;
}
