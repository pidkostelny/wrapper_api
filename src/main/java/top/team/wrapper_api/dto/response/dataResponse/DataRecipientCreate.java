package top.team.wrapper_api.dto.response.dataResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataRecipientCreate {

    private String customerId;
    private String recipientId;
    private String name;
    private String kycstatus;
    private Long walletbal;
    private LocalDate dateOfBirth;
    private String recipientName;
    private String mobileNo;

}
