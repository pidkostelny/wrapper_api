package top.team.wrapper_api.dto.response.dataResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataRecipientFetch {

    private String recipientName;
    private String recipientId;
    private String bankName;
    private String mobileNo;
    private String udf2;
    private String udf1;
    private String channel;
    private String bankCode;

}
