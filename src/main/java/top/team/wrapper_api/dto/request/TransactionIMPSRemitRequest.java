package top.team.wrapper_api.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class TransactionIMPSRemitRequest {
    @NotNull
    private String agentCode;
    @NotNull
    private String recipientId;
    @NotNull
    private String customerId;
    @NotNull
    private String amount;
    @NotNull
    private String clientRefId;
}
