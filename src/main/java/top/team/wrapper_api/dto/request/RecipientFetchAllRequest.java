package top.team.wrapper_api.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class RecipientFetchAllRequest {
    @NotNull
    private String agentCode;
    @NotNull
    private String customerId;
}
