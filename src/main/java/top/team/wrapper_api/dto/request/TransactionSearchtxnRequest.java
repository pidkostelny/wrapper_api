package top.team.wrapper_api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter@Setter
@NoArgsConstructor
public class TransactionSearchtxnRequest {
    @NotNull
    private String clientRefId;
}
