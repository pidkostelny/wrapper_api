package top.team.wrapper_api.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class TransactionNeftRequest {
    @NotNull
    private String agentCode;
    @NotNull
    private String recipientId;
    @NotNull
    private String customerId;
    @NotNull
    private String amount;
    @NotNull
    private String clientRefId;
}
