package top.team.wrapper_api.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class TransactionVerificationRequest {
    @NotNull
    private String agentCode;
    @NotNull
    private String customerId;
    @NotNull
    private String amount;
    @NotNull
    private String clientRefId;
    @NotNull
    private String udf1;
    @NotNull
    private String udf2;
}
