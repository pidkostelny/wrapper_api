package top.team.wrapper_api.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class RecipientCreateRequest {

    @NotNull
    private String agentCode;
    @NotNull
    private String bankName;
    @NotNull
    private String customerId;
    @NotNull
    private String accountNo;
    @NotNull
    private String ifsc;
    @NotNull
    private String mobileNo;
    @NotNull
    private String recipientName;

}
