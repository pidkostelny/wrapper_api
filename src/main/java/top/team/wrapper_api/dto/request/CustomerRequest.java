package top.team.wrapper_api.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class CustomerRequest {
    @NotNull
    private String agentCode;
    @NotNull
    private String customerId;
//    private String name;
//    private String address;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
//    private LocalDate dateOfBirth;
//    private String otp;

}
