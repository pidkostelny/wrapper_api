package top.team.wrapper_api.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.team.wrapper_api.dto.request.CustomerRequest;
import top.team.wrapper_api.dto.request.GenericRequest;
import top.team.wrapper_api.dto.response.*;
import top.team.wrapper_api.service.CustomerService;
import top.team.wrapper_api.service.GenericService;

import javax.validation.Valid;

@CrossOrigin
@ResponseBody
@RestController
@RequestMapping("/generic")
public class GenericController {

    @Autowired
    private GenericService genericService;

    @PostMapping("/otp")
    public GenericOtpResponse otp(@Valid @RequestBody GenericRequest request){
        return genericService.otp(request);
    }

    @GetMapping("/balance")
    public GenericBalanceResponse balance(){
        return genericService.balance();
    }

    @PostMapping("/bankList")
    public GenericBankListResponse bankList(){
        return genericService.bankList();
    }

}
