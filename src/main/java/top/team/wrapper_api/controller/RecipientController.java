package top.team.wrapper_api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.team.wrapper_api.dto.request.RecipientCreateRequest;
import top.team.wrapper_api.dto.request.RecipientFetchAllRequest;
import top.team.wrapper_api.dto.request.RecipientFetchRequest;
import top.team.wrapper_api.dto.response.*;
import top.team.wrapper_api.service.RecipientService;

import javax.validation.Valid;

@CrossOrigin
@ResponseBody
@RestController
@RequestMapping("/recipient")
public class RecipientController {

    @Autowired
    private RecipientService recipientService;

    @PostMapping("/fetchAll")
    public RecipientFetchAllResponse fetchAllRecipient(@Valid @RequestBody RecipientFetchAllRequest recipientFetchRequest){
        return recipientService.fetchAllRecipients(recipientFetchRequest);
    }

    @PostMapping("/add")
    public RecipientCreateResponse addRecipient(@Valid@RequestBody RecipientCreateRequest recipientCreateRequest){
        return recipientService.createRecipients(recipientCreateRequest);
    }

    @PostMapping("/fech")
    public RecipientFetchOneResponse fetchRecipient(@Valid@RequestBody RecipientFetchRequest recipientFetchRequest){
        return recipientService.fetchRecipient(recipientFetchRequest);
    }

    @PostMapping("/delete")
    public RecipientDeleteResponse deleteRecipient(@Valid@RequestBody RecipientFetchRequest recipientFetchRequest){
        return recipientService.deleteRecipient(recipientFetchRequest);
    }
}
