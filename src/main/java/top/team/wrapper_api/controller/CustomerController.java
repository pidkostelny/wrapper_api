package top.team.wrapper_api.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.team.wrapper_api.dto.request.CustomerCreateRequest;
import top.team.wrapper_api.dto.request.CustomerRequest;
import top.team.wrapper_api.dto.response.CustomerCreateResponse;
import top.team.wrapper_api.dto.response.CustomerFetchResponse;
import top.team.wrapper_api.service.CustomerService;

import javax.validation.Valid;

@CrossOrigin
@ResponseBody
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/create")
    public CustomerCreateResponse create(@Valid @RequestBody CustomerCreateRequest customerRequest){
        return customerService.createCustomer(customerRequest);
    }

    @PostMapping("/fetch")
    public CustomerFetchResponse fetch(@Valid@RequestBody CustomerRequest customerRequest){
        return customerService.fetchCustomer(customerRequest);
    }

}
