package top.team.wrapper_api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.team.wrapper_api.dto.request.TransactionIMPSRemitRequest;
import top.team.wrapper_api.dto.request.TransactionNeftRequest;
import top.team.wrapper_api.dto.request.TransactionSearchtxnRequest;
import top.team.wrapper_api.dto.request.TransactionVerificationRequest;
import top.team.wrapper_api.dto.response.TransactionPendingtxnlistResponse;
import top.team.wrapper_api.dto.response.TransactionResponse;
import top.team.wrapper_api.dto.response.TransactionSearchtxnResponse;
import top.team.wrapper_api.service.TransactionService;

import javax.validation.Valid;

@CrossOrigin
@ResponseBody
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/IMPS/remit")
    public TransactionResponse impsRemit(@Valid @RequestBody TransactionIMPSRemitRequest remitRequest) {
        return transactionService.impsRemitTransaction(remitRequest);
    }

    @PostMapping("/NEFT/remit")
    public TransactionResponse netfRemit(@Valid@RequestBody TransactionNeftRequest remitRequest) {
        return transactionService.neftRemitTransaction(remitRequest);
    }

    @PostMapping("/IMPS/verification")
    public TransactionResponse verification(@Valid@RequestBody TransactionVerificationRequest verificationRequest) {
        return transactionService.verificationTransaction(verificationRequest);
    }

    @PostMapping("/searchtxn")
    public TransactionSearchtxnResponse verification(@Valid@RequestBody TransactionSearchtxnRequest searchtxnRequest) {
        return transactionService.searchtxnTransaction(searchtxnRequest);
    }

    @PostMapping("/pendingtxnlist")
    public TransactionPendingtxnlistResponse pendingtxnlist() {
        return transactionService.pendingtxnlistTransaction();
    }
}
