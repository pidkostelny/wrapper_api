package top.team.wrapper_api.tool;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Log4j2
@Service
public class HttpTool {
    private static final String APPLICATION_JSON_VALUE = "application/json";

    @Value("${api.username}")
    private String username;

    @Value("${api.password}")
    private String password;

    private String authorizationValue;

    @PostConstruct
    public void init() {
        final String plainCreds = username + ":" + password;
        byte[] base64CredsBytes = Base64.encodeBase64(plainCreds.getBytes());
        authorizationValue = "Basic " + new String(base64CredsBytes);
    }

    private HttpHeaders getRequestHeader() {
        log.info(HttpHeaders.AUTHORIZATION + " -> " + authorizationValue);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE);
        requestHeaders.add(HttpHeaders.ACCEPT, APPLICATION_JSON_VALUE);
        requestHeaders.add(HttpHeaders.AUTHORIZATION, authorizationValue);
        return requestHeaders;
    }

    public <T> HttpEntity<T> getRequestHttpEntity(T object) {
        HttpHeaders requestHeaders = getRequestHeader();
        return new HttpEntity<>(object, requestHeaders);
    }
}