package top.team.wrapper_api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.team.wrapper_api.dto.request.RecipientCreateRequest;
import top.team.wrapper_api.dto.request.RecipientFetchAllRequest;
import top.team.wrapper_api.dto.request.RecipientFetchRequest;
import top.team.wrapper_api.dto.response.*;
import top.team.wrapper_api.tool.HttpTool;

@Service
public class RecipientService {

    @Value("${api.url}")
    private String apiUrl;

    @Value("${recipient.url}")
    private String baseUrl;

    @Autowired
    private HttpTool httpTool;

    public RecipientCreateResponse createRecipients(RecipientCreateRequest recipientCreateRequest) {
        return requestForCreate(recipientCreateRequest, "/add");
    }

    public RecipientFetchAllResponse fetchAllRecipients(RecipientFetchAllRequest recipient) {
        return requestForFetchAll(recipient, "/fetchAll");
    }

    public RecipientFetchOneResponse fetchRecipient(RecipientFetchRequest recipient) {
        return requestForFetch(recipient, "/fetch");
    }

    public RecipientDeleteResponse deleteRecipient(RecipientFetchRequest recipient){
        return requestForDelete(recipient, "/delete");
    }

    private RecipientCreateResponse requestForCreate(RecipientCreateRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RecipientCreateRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<RecipientCreateResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, RecipientCreateResponse.class);
        return response.getBody();
    }

    private RecipientFetchOneResponse requestForFetch(RecipientFetchRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RecipientFetchRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<RecipientFetchOneResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, RecipientFetchOneResponse.class);
        return response.getBody();
    }

    private RecipientFetchAllResponse requestForFetchAll(RecipientFetchAllRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RecipientFetchAllRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<RecipientFetchAllResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, RecipientFetchAllResponse.class);
        return response.getBody();
    }

    private RecipientDeleteResponse requestForDelete(RecipientFetchRequest recipient,  String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RecipientFetchRequest> httpEntity = httpTool.getRequestHttpEntity(recipient);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<RecipientDeleteResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, RecipientDeleteResponse.class);
        return response.getBody();
    }

}
