package top.team.wrapper_api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.team.wrapper_api.dto.request.*;
import top.team.wrapper_api.dto.response.TransactionPendingtxnlistResponse;
import top.team.wrapper_api.dto.response.TransactionResponse;
import top.team.wrapper_api.dto.response.TransactionSearchtxnResponse;
import top.team.wrapper_api.tool.HttpTool;

@Service
public class TransactionService {
    @Value("${api.url}")
    private String apiUrl;

    @Value("${transact.url}")
    private String baseUrl;

    @Autowired
    private HttpTool httpTool;

    public TransactionResponse impsRemitTransaction(TransactionIMPSRemitRequest request) {
        return requestForImpsRemit(request, "/IMPS/remit");
    }

    public TransactionResponse neftRemitTransaction(TransactionNeftRequest request) {
        return requestForNeftRemit(request, "/NEFT/remit");
    }


    public TransactionResponse verificationTransaction(TransactionVerificationRequest request) {
        return requestForVerification(request, "/IMPS/accountverification");
    }

    public TransactionSearchtxnResponse searchtxnTransaction(TransactionSearchtxnRequest request) {
        return requestForSearchtxn(request, "/searchtxn");
    }

    public TransactionPendingtxnlistResponse pendingtxnlistTransaction() {
        return requestForPendingtxnlist(new EmptyObject(),  "/pendingtxnlist");
    }

    private TransactionResponse requestForImpsRemit(TransactionIMPSRemitRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<TransactionIMPSRemitRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<TransactionResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, TransactionResponse.class);
        return response.getBody();
    }

    private TransactionResponse requestForNeftRemit(TransactionNeftRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<TransactionNeftRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<TransactionResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, TransactionResponse.class);
        return response.getBody();
    }

    private TransactionResponse requestForVerification(TransactionVerificationRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<TransactionVerificationRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<TransactionResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, TransactionResponse.class);
        return response.getBody();
    }

    private TransactionSearchtxnResponse requestForSearchtxn(TransactionSearchtxnRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<TransactionSearchtxnRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<TransactionSearchtxnResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, TransactionSearchtxnResponse.class);
        return response.getBody();
    }

    private TransactionPendingtxnlistResponse requestForPendingtxnlist(EmptyObject object, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<EmptyObject> httpEntity = httpTool.getRequestHttpEntity(object);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<TransactionPendingtxnlistResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, TransactionPendingtxnlistResponse.class);
        return response.getBody();
    }

}
