package top.team.wrapper_api.service;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.team.wrapper_api.dto.request.EmptyObject;
import top.team.wrapper_api.dto.request.GenericRequest;
import top.team.wrapper_api.dto.response.GenericBalanceResponse;
import top.team.wrapper_api.dto.response.GenericBankListResponse;
import top.team.wrapper_api.dto.response.GenericOtpResponse;
import top.team.wrapper_api.tool.HttpTool;

@Service
public class GenericService {

    @Value("${api.url}")
    private String apiUrl;

    @Value("${generic.url}")
    private String baseUrl;

    @Autowired
    private HttpTool httpTool;

    public GenericOtpResponse otp(GenericRequest genericRequest) {
        return requestForOtp(genericRequest, "/otp");
    }

    public GenericBalanceResponse balance() {
        return requestForBalance(new EmptyObject(), "/balance");
    }

    public GenericBankListResponse bankList() {
        return requestForBankList(new EmptyObject(), "/bankList");
    }

    private GenericOtpResponse requestForOtp(GenericRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<GenericRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<GenericOtpResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, GenericOtpResponse.class);
        return response.getBody();
    }

    private GenericBalanceResponse requestForBalance(EmptyObject emptyObject, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<EmptyObject> httpEntity = httpTool.getRequestHttpEntity(emptyObject);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<GenericBalanceResponse> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, GenericBalanceResponse.class);
        return response.getBody();
    }

    private GenericBankListResponse requestForBankList(EmptyObject emptyObject, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<EmptyObject> httpEntity = httpTool.getRequestHttpEntity(emptyObject);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<GenericBankListResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, GenericBankListResponse.class);
        return response.getBody();
    }

}
