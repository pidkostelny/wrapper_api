package top.team.wrapper_api.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.team.wrapper_api.dto.request.CustomerCreateRequest;
import top.team.wrapper_api.dto.request.CustomerRequest;
import top.team.wrapper_api.dto.response.CustomerCreateResponse;
import top.team.wrapper_api.dto.response.CustomerFetchResponse;
import top.team.wrapper_api.tool.HttpTool;

@Service
public class CustomerService {

    @Value("${api.url}")
    private String apiUrl;

    @Value("${customer.url}")
    private String baseUrl;

    @Autowired
    private HttpTool httpTool;

    public CustomerCreateResponse createCustomer(CustomerCreateRequest customer) {
        return requestForCreate(customer, "/create");
    }

    public CustomerFetchResponse fetchCustomer(CustomerRequest customer) {
        return requestForFetch(customer, "/fetch");
    }

    public CustomerFetchResponse requestForFetch(CustomerRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<CustomerRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<CustomerFetchResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, CustomerFetchResponse.class);
        return response.getBody();
    }
    public CustomerCreateResponse requestForCreate(CustomerCreateRequest request, String method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<CustomerCreateRequest> httpEntity = httpTool.getRequestHttpEntity(request);
        final String uri = apiUrl + baseUrl + method;
        ResponseEntity<CustomerCreateResponse> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, CustomerCreateResponse.class);
        return response.getBody();
    }
}
